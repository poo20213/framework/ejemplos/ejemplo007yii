<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Numeros */
/* @var $form ActiveForm */
?>
<div class="site-numeros">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'numero1') ?>
        <?= $form->field($model, 'numero2') ?>
        <?= $form->field($model, 'numero3') ?>
    
        <div class="form-group">
            <?= Html::submitButton($operacion, ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-numeros -->
