<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ejercicio1 */
/* @var $form ActiveForm */
?>
<div class="site-ejercicio1">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'numero') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Mostrar', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio1 -->
