<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ejercicio1;
use app\models\Ejercicio2;
use app\models\Numeros;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    
    public function actionEjercicio1() {
         $model = new Ejercicio1();

    if ($model->load(Yii::$app->request->post())) {
        if ($model->validate()) {
            // creo una variable vacia para guardar los datos del bucle
            $salida="";
            // creo un bucle for
            for($c=1;$c<=$model->numero;$c++){
                $salida=$salida . "<br>" . $c;
            }
            return $this->render('resultadoEjercicio1',[
                "salida" => $salida,
            ]);
        }
    }

    return $this->render('ejercicio1', [
        'model' => $model,
    ]);
    }
    
    public function actionEjercicio2() {
        $model = new Ejercicio2();

    if ($model->load(Yii::$app->request->post())) {
        if ($model->validate()) {
            $salida="";
            for($c=$model->numero;$c>0;$c--){
                $salida="<p>$salida $c</p>";
            }
            return $this->render('resultadoEjercicio2',[
                "salida" => $salida,
            ]);
        }
    }

    return $this->render('ejercicio2', [
        'model' => $model,
    ]);
    }
    
    public function actionEjercicio3() {
        $model = new Numeros();

    if ($model->load(Yii::$app->request->post())) {
        if ($model->validate()) {
            $resultado=$model->numero1+$model->numero2+$model->numero3;
            
            return $this->render("resultadoEjercicio3y4",[
                "resultado" => $resultado,
                "operacion" => "suma",
            ]);
        }
    }

    return $this->render('numeros', [
        'model' => $model,
        'operacion' => 'Sumar',
    ]);
    }
    
    public function actionEjercicio4() {
        $model = new Numeros();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $resultado=$model->numero1*$model->numero2*$model->numero3;

                return $this->render("resultadoEjercicio3y4",[
                    "resultado" => $resultado,
                    "operacion" => "multiplicacion",
                ]);
            }
        }

        return $this->render('numeros', [
            'model' => $model,
            'operacion' => 'Multiplicar',
        ]);
    }
}