<?php
namespace app\models;
use yii\base\Model;

class Ejercicio2 extends Model{
    public $numero;
    
    public function attributeLabels(){
        return [
            "numero" => "Numero que deseas poner"
        ];
    }
    
    public function rules() {
        return [
            [['numero'],'required','message'=>'Giliiiiiiiiiii...'],
            ['numero','compare', 'compareValue' => 10, 'operator' => '>=']
        ];
    }
}
