<?php
namespace app\models;
use yii\base\Model;

class Numeros extends Model {
    public $numero1;
    public $numero2;
    public $numero3;
    
    
    public function attributeLabels() {
        return[
            "numero1" => "Numero 1",
            "numero2" => "Numero 2",
            "numero3" => "Numero 3",
        ];
    }
    
    public function rules() {
        return [
            [['numero1','numero2','numero3'],'required','message'=>'Tienes que poner un numero cacho carne pon patas'],
            ['numero3','compare', 'compareAttribute' => 'numero2','operator'=>'>'],
            [['numero1','numero2','numero3'],'integer','message'=>'El campo {attribute} debe ser un numero'],
        ];
    }
}
