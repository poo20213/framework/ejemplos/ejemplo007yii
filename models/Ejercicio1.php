<?php
namespace app\models;
use yii\base\Model;

class Ejercicio1 extends Model{
    public $numero;
    
    public function attributeLabels(){
        return [
            "numero" => "Numero que deseas poner"
        ];
    }
    
    public function rules() {
        return [
            [['numero'],'required','message'=>'Giliiiiiiiiiii...'],
            ['numero','integer','min'=>1,'max'=>100,'message'=>'Un numero zopenco o es que no sabes leer lo que se te pide']
        ];
    }
}
